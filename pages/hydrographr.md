---
layout              : page
title               : "R package hydrographr"
meta_title          : "R package"
permalink           : "/hydrographr/"
---

The R package "hydrographr" was designed specifically for downloading and interacting with the [Hydrography90m data](/hydrography90m/hydrography90m_layers) (and soon, the 
[Environment90m data](/environment90m/environment90m_layers))!

Please check it out at [https://glowabio.github.io/hydrographr/ (documentation and vignettes)](https://glowabio.github.io/hydrographr/) !

You can view the source code on the GitHub repository, too: <br />
[https://github.com/glowabio/hydrographr](https://github.com/glowabio/hydrographr)
